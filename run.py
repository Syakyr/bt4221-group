# Imports
import parameters as par
from data import Data
from model import NNModel
import warnings
warnings.filterwarnings("ignore")

# Data 
data = Data(
    train_folder       = par.train_folder, 
    test_folder        = par.test_folder,
    image_size         = par.image_size,
    validation_percent = par.validation_percent,
    labels             = par.labels2 if par.round2 else par.labels,
    batch_size         = par.batch_size,
    width_shift_range  = par.width_shift_range,
    height_shift_range = par.height_shift_range,
    zoom_range         = par.zoom_range,
    rotation_range     = par.rotation_range,
    horizontal_flip    = par.horizontal_flip,
    vertical_flip      = par.vertical_flip
)

# Set model
# Input shape and number of classes to feed in the network will be 
# defined by the data that's being fed in
model = NNModel(
    data              = data,
    output_folder     = par.output_folder,
    epochs            = par.epochs,
    checkpoint_name   = par.checkpoint_name2 if par.round2 \
                        else par.checkpoint_name,
    use_checkpoint    = par.use_checkpoint, 
    steps_factor      = par.steps_factor,
    verbose           = par.verbose,
    learning_rate     = par.learning_rate,
    min_learning_rate = par.min_learning_rate,
    learning_rate_reduction_patience = \
        par.learning_rate_reduction_patience,
    learning_rate_reduction_factor   = \
        par.learning_rate_reduction_factor,
    early_stopping_patience          = \
        par.early_stopping_patience
)

if __name__ == '__main__':
    # Plots the model structure
    if par.plot_model: model.plot_model()

    # Run, generates and saves model to be loaded later in a notebook
    model.run()