# BT4221 Project - Image Classification for Fruits

This repository is expanded upon another repository in GitHub called 
[Fruits-360](https://github.com/Horea94/Fruit-Images-Dataset). Amongst 
many things we changed from the repository includes a refactoring of 
codes so that the codes are modularized and its code be easily swapped
for better networks/parameters.

## How to use this repository
- Add `Fruit-Images-Dataset` in this folder
- Copy `parameters-example.py` to `parameters.py` and change the 
  configurations as you see fit. The default settings are already fine.
- Check that your dependencies are available, and run `python run.py`
  to create and train the model.

## Dependencies
- `tensorflow` (Change the imports if you want to solely use `keras`)
- `scikit-learn`
- `matplotlib`
- `numpy`

## Members

- [Syakyr Surani](https://gitlab.com/Syakyr)
- [Benjamin Chew](https://gitlab.com/azchew)
- [J.W. Ling](https://gitlab.com/jwling)
- [Joshua Wang](https://gitlab.com/E0053531)
- [Ng Jian Lai](https://gitlab.com/manamisjeph)