from tensorflow.keras.utils  import plot_model as kr_plot_model
import os
from network import network, optimizer, callbacks

class NNModel:
    
    def __init__(
        self, data, output_folder, 
        epochs=25, checkpoint_name='model',
        use_checkpoint=False, verbose=True,
        steps_factor=1,
        learning_rate=0.1, min_learning_rate=0.00001, 
        learning_rate_reduction_patience=3,
        learning_rate_reduction_factor=0.5,
        early_stopping_patience=5
    ):
        
        self.data = data
        self.out_dir = output_folder
        if not os.path.exists(self.out_dir):
            os.makedirs(self.out_dir)
        self.input_shape = self.data.image_size + (3,)
        self.num_classes = len(self.data.labels)
        self.epochs = epochs
        self.chkpnt_name = checkpoint_name
        self.use_chkpnt = use_checkpoint
        self.verbose = verbose
        self.filename = None
        self.steps_factor = steps_factor
        
        self.network = network(self.input_shape, self.num_classes)
        if self.use_chkpnt: 
            filepath = os.path.join(
                self.out_dir,self.chkpnt_name
            ) + '.h5'
            self.load_weights(filepath)
            if self.verbose: print(f"Continuing from {filepath}.")
        self.optimizer = optimizer(lr=learning_rate)
        self.callbacks = callbacks(
            min_lr = min_learning_rate,
            lr_reduce_patience = learning_rate_reduction_patience,
            lr_reduce_factor = learning_rate_reduction_factor,
            es_patience = early_stopping_patience,
            chkpnt_name = os.path.join(self.out_dir, self.chkpnt_name),
            verbose = self.verbose
        )
    
    def summary(self):
        print(self.network.summary())
    
    def plot_model(self, name='model'):
        kr_plot_model(
            self.network, 
            to_file=os.path.join(self.out_dir, f"{name}.png"),
            show_shapes=True, show_layer_names=True
        )
    
    def run(self):
        self.network.compile(
            optimizer=self.optimizer,
            loss='sparse_categorical_crossentropy',
            metrics=['accuracy']
        )
        self.data.train_gen.reset()
        self.data.validation_gen.reset()
        train_gen = self.data.train_gen
        valid_gen = self.data.validation_gen
        batch_size = self.data.batch_size
        if self.verbose:
            print(f'Saving on {self.chkpnt_name}.h5.')
        self.network.fit_generator(
            generator=self.data.train_gen,
            epochs=self.epochs,
            steps_per_epoch=\
                ((train_gen.n//batch_size)+1)*self.steps_factor,
            validation_data=self.data.validation_gen,
            validation_steps=\
                ((valid_gen.n//batch_size)+1)*self.steps_factor,
            verbose=self.verbose,
            callbacks=self.callbacks
        )

    def load_weights(self, filename):
        self.filename = filename
        self.network.load_weights(filename)
        
    def predict(self, batch_type='train'):
        load = lambda gen: self.network.predict_generator(
            gen, steps=(gen.n // self.data.batch_size)+1,
            verbose=self.verbose
        )
        if batch_type == 'train':
            self.data.train_analysis_gen.reset()
            return load(self.data.train_analysis_gen)
        elif batch_type == 'test':
            self.data.test_gen.reset()
            return load(self.data.test_gen)
        else:
            raise TypeError('batch_type can only be train or test.')

    def true_values(self, batch_type='train'):
        load = lambda gen: gen.classes[gen.index_array]
        if batch_type == 'train':
            self.data.train_analysis_gen.reset()
            return load(self.data.train_analysis_gen)
        elif batch_type == 'test':
            self.data.test_gen.reset()
            return load(self.data.test_gen)
        else:
            raise TypeError('batch_type can only be train or test.')

    def update_labels(self, labels=None):
        self.data.update_labels(labels)
        self.num_classes = len(self.data.labels)
        self.network = network(self.input_shape, self.num_classes)
