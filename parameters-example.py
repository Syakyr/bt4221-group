from os.path import join
#####################################
# Data Parameters

# relative path to the Fruit-Images-Dataset folder
base_dir = 'Fruit-Images-Dataset'  
test_folder  = join(base_dir, 'Test')
train_folder = join(base_dir, 'Training')
# width and height of the used images
image_size = (100, 100)  
# percentage indicating how much of the training set should be kept for validation
validation_percent = 0.1
labels = None

#####################################
# Image Data Generator Parameters

# Number of images per batch to train/test/validate
batch_size = 50
# Steps per epoch factor. If you cannot run the whole epoch on your 
# system properly, you may need to reduce the number of steps per
# epoch.
# Note that reduced number of steps may mean that some images may not
# be underrepresented when training/validating
steps_factor = 1
width_shift_range = 0.1
height_shift_range = 0.1
zoom_range = 0.1
rotation_range = 180
horizontal_flip = True
vertical_flip = True

#####################################
# Model Parameters

# root folder in which to save the the output files
output_folder   = 'output'
epochs = 100
checkpoint_name = 'model1'
use_checkpoint = False
# controls the amount of logging done during training and testing: 
#   0 - none, 
#   1 - reports metrics after each batch, 
#   2 - reports metrics after each epoch
verbose = 1  

#####################################
# Optimization Parameters

# initial learning rate
learning_rate = 0.1  
# once the learning rate reaches this value, do not decrease it further
min_learning_rate = 0.00001  
# the factor used when reducing the learning rate -> learning_rate *= learning_rate_reduction_factor
learning_rate_reduction_factor = 0.5  
# how many epochs to wait before reducing the learning rate when the loss plateaus
learning_rate_reduction_patience = 3
early_stopping_patience = 10

# Generate a png file of the 
plot_model = False

#####################################
# 2nd round parameters

round2 = False
checkpoint_name2 = 'model2'
labels2 = None
