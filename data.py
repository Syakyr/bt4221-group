from tensorflow.keras.preprocessing.image import ImageDataGenerator
import numpy as np
import cv2, os

class Data:
    def __init__(
        self, train_folder, test_folder, image_size, 
        validation_percent=0.1, labels=None, batch_size=50,
        width_shift_range=0.0, height_shift_range=0.0,
        zoom_range=0.0, horizontal_flip=True, vertical_flip=True,
        rotation_range=0
    ):
        self.image_size = image_size
        self.train_folder = train_folder
        self.test_folder = test_folder
        self.batch_size = batch_size
        
        self.train_datagen = ImageDataGenerator(
            width_shift_range=width_shift_range,
            height_shift_range=width_shift_range,
            zoom_range=zoom_range,
            rotation_range=rotation_range,
            horizontal_flip=horizontal_flip,
            vertical_flip=vertical_flip,  # randomly flip images
            validation_split=validation_percent
        )  

        self.test_datagen = ImageDataGenerator()

        self.update_labels(labels)

    def update_labels(self, labels):
        if labels is None:
            self.labels = os.listdir(self.train_folder)
        else:
            assert type(labels) == list, "Labels must be in list form"
            self.labels = labels
        
        self.train_gen = self.train_datagen.flow_from_directory(
            self.train_folder, target_size=self.image_size, 
            class_mode='sparse', batch_size=self.batch_size, 
            shuffle=True, subset='training', classes=self.labels
        )
        self.validation_gen = self.train_datagen.flow_from_directory(
            self.train_folder, target_size=self.image_size, 
            class_mode='sparse', batch_size=self.batch_size, 
            shuffle=False, subset='validation', classes=self.labels
        )
        self.train_analysis_gen = self.test_datagen.flow_from_directory(
            self.train_folder, target_size=self.image_size, 
            class_mode='sparse', batch_size=self.batch_size, 
            shuffle=False, subset=None, classes=self.labels
        )
        self.test_gen = self.test_datagen.flow_from_directory(
            self.test_folder, target_size=self.image_size, 
            class_mode='sparse', batch_size=self.batch_size, 
            shuffle=False, subset=None, classes=self.labels
        )