from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input, Dense, Dropout, Activation
from tensorflow.keras.layers import Flatten, Conv2D, MaxPooling2D
from tensorflow.keras.layers import Lambda
from tensorflow.keras.optimizers import Adadelta
from tensorflow.keras.callbacks import EarlyStopping, ReduceLROnPlateau
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.image import rgb_to_hsv, rgb_to_grayscale
from tensorflow import concat as tf_concat

#####################################
# Model

def network(input_shape, num_classes):
    img_input = Input(shape=input_shape, name='data')
    #x = Lambda(image_process)(img_input)
    x = Lambda(
        lambda x: tf_concat(
            [rgb_to_hsv(x), rgb_to_grayscale(x)], axis=-1
        )
    )(img_input)
    x = Conv2D(
        16, (5, 5), strides=(1, 1), padding='same', name='conv1'
    )(x)
    x = Activation('relu', name='conv1_relu')(x)
    x = MaxPooling2D(
        (2, 2), strides=(2, 2), padding='valid', name='pool1'
    )(x)
    x = Conv2D(
        32, (5, 5), strides=(1, 1), padding='same', name='conv2'
    )(x)
    x = Activation('relu', name='conv2_relu')(x)
    x = MaxPooling2D(
        (2, 2), strides=(2, 2), padding='valid', name='pool2'
    )(x)
    x = Conv2D(
        64, (5, 5), strides=(1, 1), padding='same', name='conv3'
    )(x)
    x = Activation('relu', name='conv3_relu')(x)
    x = MaxPooling2D(
        (2, 2), strides=(2, 2), padding='valid', name='pool3'
    )(x)
    x = Conv2D(
        128, (5, 5), strides=(1, 1), padding='same', name='conv4'
    )(x)
    x = Activation('relu', name='conv4_relu')(x)
    x = MaxPooling2D(
        (2, 2), strides=(2, 2), padding='valid', name='pool4'
    )(x)
    x = Flatten()(x)
    x = Dense(1024, activation='relu', name='fcl1')(x)
    x = Dropout(0.2)(x)
    x = Dense(128, activation='relu', name='fcl2')(x)
    x = Dropout(0.2)(x)
    out = Dense(
        num_classes, activation='softmax', name='predictions'
    )(x)
    res = Model(inputs=img_input, outputs=out)
    return res

#####################################
# Optimizer

optimizer = Adadelta

#####################################
# Callbacks

def callbacks(
    min_lr=0.00001, lr_reduce_patience=3,
    lr_reduce_factor=0.5, es_patience=5,
    chkpnt_name='model', verbose=True
):
    return [
        ReduceLROnPlateau(
            monitor='val_loss', patience=lr_reduce_patience,
            verbose=verbose, factor=lr_reduce_factor, min_lr=min_lr
        ),
        EarlyStopping(
            monitor='val_loss', mode='min', verbose=verbose,
            patience=es_patience
        ),
        ModelCheckpoint(
            filepath=f'{chkpnt_name}.h5', monitor='val_acc', 
            verbose=verbose, save_best_only=True, 
            save_weights_only=False, mode='max', period=1
        )
    ]